# uppsala-big-data-meetup.gitlab.io

Simple website for the [Uppsala Big Data Meetup](https://www.meetup.com/Uppsala-Big-Data-Meetup/),
using the [mindoc](https://bitfragment.net/mindoc/) template.
